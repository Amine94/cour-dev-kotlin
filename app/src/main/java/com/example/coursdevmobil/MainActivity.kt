package com.example.coursdevmobil

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.speech.RecognizerIntent
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import java.lang.Exception
import java.util.*

class MainActivity : AppCompatActivity() {


    var bol1 = false
    var bol2 = false
    var bol3 = false


    val  REQUEST_CODE_SPEECH_INPUT=100



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        txt_1.text=""
        txt_2.text="Sa"
        txt_3.text="lut"

        cadre_txt1.setBackgroundColor(Color.WHITE)

        btn.setOnClickListener{

            speak()
        }


    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        val string = (txt_1.text.toString()+txt_2.text.toString()+txt_3.text.toString()).toUpperCase()

        when(requestCode){
            REQUEST_CODE_SPEECH_INPUT-> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    val result = (data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS))[0].toString().toUpperCase()
                    if(string==result){
                        if (bol1==false && bol2==false && bol3==false ){
                            bol1=true
                            txt_1.text="Je "
                            txt_2.text="suis"
                            txt_3.text=""
                        }
                        else if(bol1==true && bol2==false && bol3==false){
                            bol2=true
                            txt_1.text="Le "
                            txt_2.text="re"
                            txt_3.text="nard"
                        }
                        else if(bol1==true && bol2==true && bol3==false){
                            bol3=true

                            cadre_txt2.setBackgroundColor(Color.WHITE)
                            txt_1.text="de"
                            txt_2.text=""
                            txt_3.text="la"
                            txt_4.text="fo"
                            txt_5.text="rêt"
                            
                        }
                    }
                    Toast.makeText(this,"Bien joué",Toast.LENGTH_SHORT).show()
                }
                else
                    Toast.makeText(this,"Pas correct",Toast.LENGTH_SHORT).show()

            }
        }
    }





    private fun speak() {

        val speakIntent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)

        //speakIntent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS,999999999999999999)
        speakIntent.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
        speakIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
        speakIntent.putExtra(RecognizerIntent.EXTRA_PROMPT, "parlez")

        try{
            startActivityForResult(speakIntent,REQUEST_CODE_SPEECH_INPUT)
        }catch (e: Exception){
            Toast.makeText(this,e.message, Toast.LENGTH_SHORT).show()
        }

    }

}


